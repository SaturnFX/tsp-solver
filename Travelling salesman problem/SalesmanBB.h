#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"

/*
* Moduł SalesmanBB
*
* Zawiera implementację algorytmu rozwiązującego 
* asymetryczny problem komiwojażera (aTSP)
* typu branch and bound
*/

//Funkcja rozwiązująca asymetryczny problem komiwojażera (aTSP)
//metodą podziału i ograniczeń (ang. branch and bound)
int SalesmanBranchBoundSolver(const SalesmanCities& InputData, LinkedList<int>& OutputRoute);