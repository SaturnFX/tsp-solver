#pragma once
#include "Global.h"
#include "AbstractContainer.h"
#include "Iterator.h"
#include "Exception.h"

/*
* Klasa: LinkedList
*
* Klasa b�d�ca implementacj� podw�jnie
* wi�zanej listy wraz z obs�ug� iteratora
* 
* UWAGA! Poniewa� jest to klasa szablonowa
* musi by� ona zaimplementowana w ca�o�ci
* w pliku nag��wkowym
*/
template <typename T> 
class LinkedList : public AbstractContainer
{
private:
	//Definicja struktury zawieraj�cej
	//pojedy�czy element listy
	struct ItemData
	{
		T Value;
		ItemData* NextItem;
		ItemData* PreviousItem;

		ItemData(T ItemValue)
		{
			Value = ItemValue;
			NextItem = nullptr;
			PreviousItem = nullptr;
		}
	} *ListBegin, *ListEnd;

	//Procedura inicjuj�ca list�
	void InitializeList()
	{
		ListBegin = new ItemData(T());
		ListEnd = new ItemData(T());
		InitializeEmptyConnections();
	}

	//Procedura inicjuj�ca pocz�tkowe
	//powi�zania pomi�dzy elementami
	void InitializeEmptyConnections()
	{
		ListBegin->NextItem = ListEnd;
		ListBegin->PreviousItem = ListBegin;
		ListEnd->NextItem = ListEnd;
		ListEnd->PreviousItem = ListBegin;
	}

	//Funkcja wstawiaj�ca dany element
	//w miejsce poprzedniego
	ItemData* InsertItem(ItemData* ExistingItem, T Value)
	{
		ItemData* NewItem = new ItemData(Value);
		NewItem->PreviousItem = ExistingItem->PreviousItem;
		if (ExistingItem == ListBegin)
		{
			NewItem->NextItem = ExistingItem->NextItem;
			ExistingItem->NextItem->PreviousItem = NewItem;
			ExistingItem->NextItem = NewItem;
		}
		else
		{
			NewItem->NextItem = ExistingItem;
			ExistingItem->PreviousItem->NextItem = NewItem;
			ExistingItem->PreviousItem = NewItem;
		}
		++DataLength;
		return NewItem;
	}

	//Funkcja usuwaj�ca dany element
	ItemData* RemoveItem(ItemData* ExistingItem)
	{
		if ((ExistingItem == ListBegin) || (ExistingItem == ListEnd))
		{
			return ExistingItem;
		}
		ItemData* NextItem = ExistingItem->NextItem;
		ExistingItem->PreviousItem->NextItem = NextItem;
		NextItem->PreviousItem = ExistingItem->PreviousItem;
		delete ExistingItem;
		--DataLength;
		return NextItem;
	}

	//Funkcja czyszcz�ca list�
	void ClearList()
	{
		if (DataLength == 0) return;
		ItemData* CurrentItem = ListBegin->NextItem;
		ItemData* NextItem = CurrentItem->NextItem;
		while (CurrentItem != NextItem)
		{
			delete CurrentItem;
			CurrentItem = NextItem;
			NextItem = CurrentItem->NextItem;
		}
		AbstractContainer::Clear();
	}

public:
	//Definicja iteratora listy
	class Iterator : public AbstractIterator<T>
	{
		friend class LinkedList<T>;

	private:
		LinkedList<T>* Container;
		ItemData* CurrentItem;

		Iterator(LinkedList<T>* ContainerList, ItemData* Item) 
			: Container(ContainerList), CurrentItem(Item)
		{}

		//Metoda sprawdzaj�ca czy iterator jest prawid�owy
		//je�li nie zwracany jest wyj�tek
		void ValidateIterator() const
		{
			if (Container != nullptr) return;
			throw ContainerException(std::string("This iterator is not assigned to any container"));
		}

	public:
		Iterator() 
			: Container(nullptr), CurrentItem(nullptr)
		{}

		Iterator(const Iterator& ExistingIterator)
			:Container(ExistingIterator.Container),
			CurrentItem(ExistingIterator.CurrentItem)
		{}

		//Funkcja sprawdzaj�ca czy iterator
		//jest na pocz�tku listy
		bool IsAtTheStart() const
		{
			ValidateIterator();
			return (CurrentItem == Container->ListBegin);
		}
		
		bool IsAtTheEnd() const
		{
			ValidateIterator();
			return (CurrentItem == Container->ListEnd);		
		}

		T GetValue() const
		{
			return CurrentItem->Value;
		}

		//Procedura dodaj�ca element 
		//w miejscu iteratora
		void Add(T Value)
		{
			ValidateIterator();
			CurrentItem = 
				Container->InsertItem(CurrentItem, Value);
		}

		//Procedura usuwaj�ca element 
		//znajduj�cy si� w miejscu iteratora
		void Remove()
		{
			ValidateIterator();
			CurrentItem = 
				Container->RemoveItem(CurrentItem);
		}

		void GoToNext()
		{
			ValidateIterator();
			CurrentItem = CurrentItem->NextItem;
		}

		//Procedura przechodz�ca
		//o jedn� pozycj� w ty�
		void GoToPrevious()
		{
			ValidateIterator();
			CurrentItem = CurrentItem->PreviousItem;
		}

		//Przeci��enie operatora przypisania
		Iterator& operator =(Iterator& OtherIterator)
		{
			Container = OtherIterator.Container;
			CurrentItem = OtherIterator.CurrentItem;
			return *this;
		}

		//Przeci��enie operatora por�wnania
		bool operator ==(const Iterator& OtherIterator) const
		{
			return (CurrentItem == OtherIterator.CurrentItem);
		}

		//Przeci��enie operatora wy�uskania
		T& operator *()
		{
			ValidateIterator();
			return CurrentItem->Value;
		}

		//Przeci��enie operatora preinkrementacji
		Iterator& operator ++()
		{
			GoToNext();
			return *this;
		}

		//Przeci��enie operatora postinkrementacji
		Iterator operator ++(int)
		{
			Iterator ObjectCopy(*this); 
			GoToNext();
			return ObjectCopy;
		}

		//Przeci��enie operatora predekrementacji
		Iterator& operator --()
		{
			GoToPrevious();
			return *this;
		}

		//Przeci��enie operatora postdekrementacji
		Iterator operator --(int)
		{
			Iterator ObjectCopy(*this); 
			GoToPrevious();
			return ObjectCopy;
		}
	};

	//Definicja iteratora listy dzia�aj�cego 
	//w trybie read-only
	class ReadOnlyIterator : public AbstractIterator<T>
	{
		friend class LinkedList<T>;

	private:
		const LinkedList<T>* Container;
		const ItemData* CurrentItem;

		ReadOnlyIterator(const LinkedList<T>* ContainerList, const ItemData* Item) 
			: Container(ContainerList), CurrentItem(Item)
		{}

		void ValidateIterator() const
		{
			if (Container != nullptr) return;
			throw ContainerException(std::string("This iterator is not assigned to any container"));
		}

	public:
		ReadOnlyIterator() 
			: Container(nullptr), CurrentItem(nullptr)
		{}

		ReadOnlyIterator(const ReadOnlyIterator& ExistingIterator)
			:Container(ExistingIterator.Container),
			CurrentItem(ExistingIterator.CurrentItem)
		{}

		bool IsAtTheStart() const
		{
			ValidateIterator();
			return (CurrentItem == Container->ListBegin);
		}
		
		bool IsAtTheEnd() const
		{
			ValidateIterator();
			return (CurrentItem == Container->ListEnd);		
		}

		T GetValue() const
		{
			return CurrentItem->Value;
		}

		void GoToNext()
		{
			ValidateIterator();
			CurrentItem = CurrentItem->NextItem;
		}

		void GoToPrevious()
		{
			ValidateIterator();
			CurrentItem = CurrentItem->PreviousItem;
		}

		ReadOnlyIterator& operator =(ReadOnlyIterator& OtherIterator)
		{
			Container = OtherIterator.Container;
			CurrentItem = OtherIterator.CurrentItem;
			return *this;
		}

		bool operator ==(const ReadOnlyIterator& OtherIterator) const
		{
			return (CurrentItem == OtherIterator.CurrentItem);
		}

		const T& operator *()
		{
			ValidateIterator();
			return CurrentItem->Value;
		}

		ReadOnlyIterator& operator ++()
		{
			GoToNext();
			return *this;
		}

		ReadOnlyIterator operator ++(int)
		{
			ReadOnlyIterator ObjectCopy(*this); 
			GoToNext();
			return ObjectCopy;
		}

		ReadOnlyIterator& operator --()
		{
			GoToPrevious();
			return *this;
		}

		ReadOnlyIterator operator --(int)
		{
			ReadOnlyIterator ObjectCopy(*this); 
			GoToPrevious();
			return ObjectCopy;
		}
	};

	LinkedList()
	{
		InitializeList();
	}

	//Tzw. copy-constructor potrzebny
	//np. do przekazywania listy jako 
	//argument do funkcji
	LinkedList(const LinkedList& SourceList)
	{
		InitializeList();
		Load(SourceList);
	}

	//Funkcja zwracaj�ca pierwszy element
	//listy
	T GetFirst() const
	{
		ValidateLength();
		return ListBegin->NextItem->Value;
	}

	//Funkcja zwracaj�ca ostatni element
	//listy
	T GetLast() const
	{
		ValidateLength();
		return ListEnd->PreviousItem->Value;
	}
	
	//Funkcja zwracaj�ca iterator
	//w pozycji pocz�tkowej
	Iterator GetStartIterator()
	{
		return Iterator(this, ListBegin->NextItem);
	}

	//Funkcja zwracaj�ca iterator
	//w pozycji ko�cowej
	Iterator GetEndIterator()
	{
		return Iterator(this, ListEnd->PreviousItem);
	}

	//Funkcja zwracaj�ca iterator
	//tylko do odczytu w pozycji pocz�tkowej
	ReadOnlyIterator GetReadOnlyStartIterator() const
	{
		return ReadOnlyIterator(this, ListBegin->NextItem);
	}

	//Funkcja zwracaj�ca iterator
	//tylko do odczytu w pozycji ko�cowej
	ReadOnlyIterator GetReadOnlyEndIterator() const
	{
		return ReadOnlyIterator(this, ListEnd->PreviousItem);
	}

	//Metoda za�adowywuj�ca list�
	//elementami z podanej tablicy
	void Load(const T* InputData, int Length)
	{
		for (int CurrentIndex = 0; CurrentIndex < Length; ++CurrentIndex)
		{
			AddLast(InputData[CurrentIndex]);
		}
	}

	//Metoda za�adowywuj�ca list�
	//elementami z innej listy
	void Load(const LinkedList& InputList)
	{
		ReadOnlyIterator InputIterator = InputList.GetReadOnlyStartIterator();
		while (!InputIterator.IsAtTheEnd())
		{
			AddLast(InputIterator.GetValue());
			InputIterator.GoToNext();
		}
	}

	//Metoda dodaj�ca element
	//na pocz�tku listy
	void AddFirst(T Value)
	{
		InsertItem(ListBegin, Value);
	}

	//Metoda dodaj�ca element
	//na ko�cu listy
	void AddLast(T Value)
	{
		InsertItem(ListEnd, Value);
	}

	//Metoda usuwaj�ca pierwszy
	//element listy
	void RemoveFirst()
	{
		ValidateLength();
		RemoveItem(ListBegin->NextItem);
	}

	//Metoda usuwaj�ca ostatni
	//element listy
	void RemoveLast()
	{
		ValidateLength();
		RemoveItem(ListEnd->PreviousItem);
	}

	//Metoda czyszcz�ca list�
	void Clear()
	{
		ClearList();
		InitializeEmptyConnections();
	}

	//Przeci��enie operatora przypisania
	LinkedList& operator=(const LinkedList& ExistingList)
	{
		if (this == &ExistingList) return *this;
		Clear();
		Load(ExistingList);
		return *this;
	}

	~LinkedList()
	{
		ClearList();
		delete ListBegin;
		delete ListEnd;
	}
};
