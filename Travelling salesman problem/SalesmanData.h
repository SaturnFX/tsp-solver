#pragma once
#include "Global.h"

/*
* Klasa: SalesmanCities
*
* Zawiera opis miast w postaci grafu
* pe�ni�cego rol� danych wej�ciowych
* dla algorytm�w rozwi�zuj�cych
* problem komiwoja�era
*/

struct SalesmanCities
{
	int CitiesCount;
	int** CitiesDistances;

	SalesmanCities(int InputCount);
	SalesmanCities(const SalesmanCities& ExistingCitiesData);
	~SalesmanCities();
};