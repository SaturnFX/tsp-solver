#include "Global.h"
#include "Exception.h"

ApplicationException::ApplicationException(const std::string Description) : Mesage(Description) {}

const char* ApplicationException::what() const throw () 
{
    return Mesage.c_str();
}