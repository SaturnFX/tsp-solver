#pragma once
#include "Global.h"

/*
* Klasa: AbstractIterator
*
* Klasa bazowa dla wszystkich rozdzaj�w
* iterator�w
*/
template <typename T> 
class AbstractIterator
{
public:
	AbstractIterator() {};
	//Funkcja zwracaj�ca true je�eli
	//iterator znajduje si� na ko�cu 
	//iterowanej sekwencji
	virtual bool IsAtTheEnd() const = 0;
	//Funkcja pobieraj�ca aktualn� warto��
	virtual T GetValue() const = 0;
	//Funkcja przesuwaj�ca iterator o
	//jedn� pozycj� do przodu
	virtual void GoToNext() = 0;
};
