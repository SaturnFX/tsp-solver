#pragma once
#include "Global.h"
#include "LinkedList.h"

/*
* Modu� SalesmanCommon
*
* Zawiera funkcje wykorzystywane
* w wielu algorytmach rozwi�zuj�cych
* problem komiwoja�era
*/

//Funkcja konwertuj�ca tablic� po��cze�
//trasy na list� miast
inline void ConvertRouteConnectionsToList(const int* CitiesConnection, 
	LinkedList<int>& OutputRoute)
{
	OutputRoute.AddLast(0);
	int CurrentCity = CitiesConnection[0];
	while (CurrentCity)
	{
		OutputRoute.AddLast(CurrentCity);
		CurrentCity = CitiesConnection[CurrentCity];
	}
	OutputRoute.AddLast(0);
}