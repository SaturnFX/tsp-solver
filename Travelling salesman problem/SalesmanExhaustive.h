#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"

/*
* Moduł SalesmanExhaustive
*
* Zawiera implementację algorytmu rozwiązującego 
* asymetryczny problem komiwojażera (aTSP)
* typu brute-force
*/

//Funkcja rozwiązująca asymetryczny problem komiwojażera (aTSP)
//metodą przeszukiwania wyczerpującego 
int SalesmanExhaustiveSearch(const SalesmanCities& InputData, LinkedList<int>& OutputRoute);