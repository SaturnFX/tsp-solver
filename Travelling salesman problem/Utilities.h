#pragma once
#include "Global.h"

/*
* Modu� Utilities
*
* Zawiera procedury pomocnicze
*/

//Procedura zamiany element�w
template <typename T>
void SwapItems(T* ArrayData, int FirstIndex, int SecondIndex)
{
	T TemporaryValue = ArrayData[FirstIndex];
	ArrayData[FirstIndex] = ArrayData[SecondIndex];
	ArrayData[SecondIndex] = TemporaryValue;
}

//Funkcja ustawiaj�ca ziarno generatora liczb
//pseudolosowych
void SeedRandomGenerator();

//Funkcja zwracaj�ca losow� warto�� z przedzia�u
int GetRandomInteger(int MinimumValue, int MaximumValue);

//Funkcja obliczaj�ca silni� dla danej
//liczby ca�kowitej
int CalculateFactorial(int InputNumber);