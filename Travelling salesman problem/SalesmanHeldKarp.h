#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "Exception.h"
#include "LinkedList.h"

/*
* Moduł SalesmanHeldKarp
*
* Zawiera implementację algorytmu rozwiązującego 
* asymetryczny problem komiwojażera (aTSP)
* przy użyciu techniki programowania dynamicznego
*/

//Funkcja realizująca algorytm Helda-Karpa rozwiązującego aTSP
//będącego przykładem zastosowania metody programowania dynamicznego
int SalesmanHeldKarpSolver(const SalesmanCities& InputData, LinkedList<int>& OutputRoute);