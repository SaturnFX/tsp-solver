#include "Global.h"
#include "SalesmanBB.h"
#include "SalesmanCommon.h"
#include "PriorityQueue.h"
#include "Utilities.h"

#include <limits>

//Struktura zawierająca opis
//pojedyńczego odcinka trasy komiwojażera
struct SalesmanSegment
{
	int FromCity;
	int ToCity;	
	int Distance;

	SalesmanSegment();
	SalesmanSegment(int FromValue, int ToValue, int DistanceValue);
};

//Struktura reprezentująca kompletne
//trasy lub ich fragmenty przetwarzane
//przez algorytm Branch and Bound
struct SalesmanBBRoute
{
	int MinimumDistance;
	int FixedDistance;
	bool IsComplete;
	int* FixedConnections;
	int NextFromCity;
	LinkedList<int> ToCities;
	int* PossibleLoops;

	//Konstruktor tworzący instancje 
	//struktury z pustą trasą
	SalesmanBBRoute(int CitiesCount);
	//Konstruktor kopiujący zawartość struktury
	//z już istniejącej. Jest częścią realizacji
	//operacji "branch" w algorytmie.
	SalesmanBBRoute(const SalesmanBBRoute& ExistingItem, int CitiesCount);
	//Metoda aktualizująca dolne oszacowanie
	//długości trasy w algorytmie - realizacja operacji "bound".
	void UpdateDistance(const SalesmanCities& InputData);
	~SalesmanBBRoute();
};

//Funkcja porównująca struktury znajdujące
//się w kolejce priorytetowej
bool CompareRoutes(SalesmanBBRoute* const & FirstItem, 
	SalesmanBBRoute* const & SecondItem);

int SalesmanBranchBoundSolver(const SalesmanCities& InputData, LinkedList<int>& OutputRoute)
{
	SalesmanCities CitiesData(InputData);
	for (int CityIndex = 0; CityIndex < CitiesData.CitiesCount; ++CityIndex)
	{
		CitiesData.CitiesDistances[CityIndex][CityIndex] = std::numeric_limits<int>::max();
	}
	SalesmanBBRoute* CurrentRoute = nullptr;
	SalesmanBBRoute* ChildRoute = nullptr;
	LinkedList<int>::Iterator EndCitiesIterator;
	int StartCity = 0;
	int EndCity = 0;
	int LastButOneCity = CitiesData.CitiesCount - 2;
	bool IsLastButOneCity = false;
	int CurrentDistance = 0;
	int StartCityCycle = 0;
	int EndCityCycle = 0;
	int LastCityStart = 0;
	int LastCityEnd = 0;
	SalesmanSegment RestoredSegment;
	LinkedList<SalesmanSegment> SegmentsBackup;
	PriorityQueue<SalesmanBBRoute*> RoutesQueue(
		CalculateFactorial(CitiesData.CitiesCount - 1), CompareRoutes);
	CurrentRoute = new SalesmanBBRoute(CitiesData.CitiesCount);
	CurrentRoute->UpdateDistance(CitiesData);
	RoutesQueue.Add(CurrentRoute);
	while (!RoutesQueue.IsEmpty())
	{
		CurrentRoute = RoutesQueue.ExtractMinimum();
		//Sprawdzenie czy dany element nie jest
		//najlepszym rozwiązaniem
		if (CurrentRoute->IsComplete)
		{
			break;
		}
		//Miasto opuszczane przez komiwojażera
		StartCity = CurrentRoute->NextFromCity;
		IsLastButOneCity = (StartCity == LastButOneCity);
		++CurrentRoute->NextFromCity;
		EndCitiesIterator = CurrentRoute->ToCities.GetStartIterator();
		while (!EndCitiesIterator.IsAtTheEnd())
		{
			//Miasto, do którego dociera komiwojażer
			EndCity = *EndCitiesIterator;
			if ((StartCity == EndCity) || (CurrentRoute->PossibleLoops[StartCity] == EndCity))
			{
				++EndCitiesIterator;
				continue;
			}
			EndCitiesIterator.Remove();
			CurrentDistance = CitiesData.CitiesDistances[StartCity][EndCity];
			ChildRoute = new SalesmanBBRoute(*CurrentRoute, CitiesData.CitiesCount);
			ChildRoute->FixedConnections[StartCity] = EndCity;
			ChildRoute->FixedDistance += CurrentDistance;
			ChildRoute->IsComplete = IsLastButOneCity;
			if (!IsLastButOneCity)
			{
				//Zaaktualizowanie tablicy połączeń w celu
				//eliminacji podtras (ang. subtours)
				StartCityCycle = ChildRoute->PossibleLoops[StartCity];
				if (StartCityCycle < 0) 
				{
					StartCityCycle = StartCity;
				}
				else
				{
					ChildRoute->PossibleLoops[StartCity] = -1;
				}
				EndCityCycle = ChildRoute->PossibleLoops[EndCity];
				if (EndCityCycle < 0) 
				{
					EndCityCycle = EndCity;
				}
				else
				{
					ChildRoute->PossibleLoops[EndCity] = -1;
				}
				ChildRoute->PossibleLoops[StartCityCycle] = EndCityCycle;
				ChildRoute->PossibleLoops[EndCityCycle] = StartCityCycle;
				//Tymczasowe zablokowanie wliczania do minimum
				//odcinków powodujących powstanie podtras.
				for (StartCityCycle = 0; StartCityCycle < CitiesData.CitiesCount; ++StartCityCycle)
				{
					EndCityCycle = ChildRoute->PossibleLoops[StartCityCycle];
					if (EndCityCycle < 0) continue;
					SegmentsBackup.AddLast(SalesmanSegment(StartCityCycle, EndCityCycle, 
						CitiesData.CitiesDistances[StartCityCycle][EndCityCycle]));
					CitiesData.CitiesDistances[StartCityCycle][EndCityCycle] = std::numeric_limits<int>::max();
				}
			}
			else
			{
				//Dodanie ostatniego pozostałego
				//odcinka trasy komiwojażera
				LastCityStart = ChildRoute->NextFromCity++;
				LastCityEnd = ChildRoute->ToCities.GetFirst();
				ChildRoute->ToCities.RemoveFirst();
				CurrentDistance = CitiesData.CitiesDistances[LastCityStart][LastCityEnd];
				ChildRoute->FixedConnections[LastCityStart] = LastCityEnd;
				ChildRoute->FixedDistance += CurrentDistance;
			}
			ChildRoute->UpdateDistance(CitiesData);
			while (!SegmentsBackup.IsEmpty())
			{
				RestoredSegment = SegmentsBackup.GetFirst();
				SegmentsBackup.RemoveFirst();
				CitiesData.CitiesDistances[RestoredSegment.FromCity][RestoredSegment.ToCity] = RestoredSegment.Distance;
			}
			RoutesQueue.Add(ChildRoute);
			EndCitiesIterator.Add(EndCity);
			++EndCitiesIterator;
		}
		delete CurrentRoute;
	}
	while (!RoutesQueue.IsEmpty())
	{
		delete RoutesQueue.ExtractMinimum();
	}
	int OutputDistance = CurrentRoute->FixedDistance;
	ConvertRouteConnectionsToList(CurrentRoute->FixedConnections, OutputRoute);
	delete CurrentRoute;
	return OutputDistance;
}

SalesmanBBRoute::SalesmanBBRoute(int CitiesCount)
	:MinimumDistance(0), FixedDistance(0), IsComplete(false), 
	FixedConnections(new int[CitiesCount]()), NextFromCity(0), 
	PossibleLoops(new int[CitiesCount])
{
	for (int CityIndex = 0; CityIndex < CitiesCount; ++CityIndex)
	{
		ToCities.AddLast(CityIndex);
		PossibleLoops[CityIndex] = -1;
	}
}

SalesmanBBRoute::SalesmanBBRoute(const SalesmanBBRoute& ExistingItem, int CitiesCount)
	:MinimumDistance(ExistingItem.MinimumDistance), FixedDistance(ExistingItem.FixedDistance),
	IsComplete(ExistingItem.IsComplete), FixedConnections(new int[CitiesCount]), 
	NextFromCity(ExistingItem.NextFromCity), ToCities(ExistingItem.ToCities),
	PossibleLoops(new int[CitiesCount])
{
	std::copy(ExistingItem.FixedConnections, 
		ExistingItem.FixedConnections + CitiesCount, FixedConnections);
	std::copy(ExistingItem.PossibleLoops, 
		ExistingItem.PossibleLoops + CitiesCount, PossibleLoops);
}

void SalesmanBBRoute::UpdateDistance(const SalesmanCities& InputData)
{
	int TotalValue = 0;
	int CurrentMinimum = 0;
	int* CurrentRowArray = nullptr;
	LinkedList<int>::ReadOnlyIterator ToIterator 
		= ToCities.GetReadOnlyStartIterator();
	for (int FromIndex = NextFromCity; FromIndex < InputData.CitiesCount; ++FromIndex)
	{
		CurrentRowArray = InputData.CitiesDistances[FromIndex];
		CurrentMinimum = CurrentRowArray[*ToIterator++];
		while (!ToIterator.IsAtTheEnd())
		{
			if (CurrentRowArray[*ToIterator] < CurrentMinimum)
			{
				CurrentMinimum = CurrentRowArray[*ToIterator];
			}
			++ToIterator;
		}
		TotalValue += CurrentMinimum;
		ToIterator = ToCities.GetReadOnlyStartIterator();
	}
	MinimumDistance = FixedDistance + TotalValue;
}

SalesmanBBRoute::~SalesmanBBRoute()
{
	delete[] FixedConnections;
	delete[] PossibleLoops;
}

SalesmanSegment::SalesmanSegment()
	:FromCity(0), ToCity(0), Distance(0)
{}

SalesmanSegment::SalesmanSegment(int FromValue, int ToValue, int DistanceValue)
	:FromCity(FromValue), ToCity(ToValue), Distance(DistanceValue)
{}

bool CompareRoutes(SalesmanBBRoute* const & FirstItem, 
	SalesmanBBRoute* const & SecondItem)
{
	int DistanceDifference = FirstItem->MinimumDistance
		- SecondItem->MinimumDistance;
	return ((DistanceDifference < 0) || ((DistanceDifference == 0) 
		&& (FirstItem->IsComplete && !SecondItem->IsComplete)));
}