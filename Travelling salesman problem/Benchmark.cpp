#include "Global.h"
#include "Benchmark.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "SalesmanIO.h"
#include "SalesmanExhaustive.h"
#include "SalesmanBB.h"
#include "SalesmanHeldKarp.h"
#include "Timer.h"

#include <iomanip>

#define CITIES_DISTANCE_MINIMUM 1
#define CITIES_DISTANCE_MAXIMUM 100

//Przeprowadzanie pomiar�w wydajno�ci ka�dego z zaimplementowanych
//algorytm�w rozwi�zuj�cego TSP dla okre�lonego rozmiaru danych wej�ciowych 
//oraz liczby iteracji 
void BenchmarkSalesmanInputSize(int CitiesCount, int RepeatsCount, bool SkipExhaustive);
//Przeprowadzenie pomiaru �redniego czasu wykonania danego algorytmu
//TSP dla podanej ilo�ci miast i liczby powt�rze�
void BenchmarkSalesmanAlgorithm(int (*SalesmanFunction)(const SalesmanCities&, LinkedList<int>&),
	int CitiesCount, int RepeatsCount);

void BenchmarkSalesman(int RepeatsCount)
{
	Timer AllBenchmarksTimer;
	static const int SalesmanSizes[] = {3, 5, 7, 9, 10, 11, 12, 13};
	static const bool SalesmanSkipExhaustive[] = {false, false, false, false, false, false, false, true};
	static const int SalesmanSizesCount = sizeof(SalesmanSizes) / sizeof(*SalesmanSizes);
	std::cout << "Testowanie wydajno\x98""ci dla " << RepeatsCount << " powt\xA2rze\xE4:" << std::endl;
	std::cout << std::endl;
	std::cout << "             Atak si\x88owy    Branch & Bound         Held-Karp" << std::endl;
	std::cout << "     \xDA"
		"\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4" 
		"\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4"
		"\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4" << std::endl;
	AllBenchmarksTimer.StartTimer();
	for (int SizeIndex = 0;  SizeIndex < SalesmanSizesCount; ++SizeIndex)
	{
		BenchmarkSalesmanInputSize(SalesmanSizes[SizeIndex], 
			RepeatsCount, SalesmanSkipExhaustive[SizeIndex]);
	}
	AllBenchmarksTimer.StopTimer();
	long long int AllBenchmarksTime = AllBenchmarksTimer.GetMiliseconds() / 1000;
	std::cout << std::endl;
	std::cout << "Ca\x88kowity czas testowania: " 
		<< AllBenchmarksTime / 60 << " min " 
		<< AllBenchmarksTime % 60 << " s" << std::endl;
}

void BenchmarkSalesmanInputSize(int CitiesCount, int RepeatsCount, bool SkipExhaustive)
{
	std::cout << std::setw(5) << CitiesCount << "\xB3";
	std::cout << std::setw(15);
	if (!SkipExhaustive)
	{
		BenchmarkSalesmanAlgorithm(&SalesmanExhaustiveSearch, CitiesCount, RepeatsCount);
	}
	else
	{
		std::cout << "-" << " us";
	}
	std::cout << std::setw(15);
	BenchmarkSalesmanAlgorithm(&SalesmanBranchBoundSolver, CitiesCount, RepeatsCount);
	std::cout << std::setw(15);
	BenchmarkSalesmanAlgorithm(&SalesmanHeldKarpSolver, CitiesCount, RepeatsCount);
	std::cout << std::endl;
}

void BenchmarkSalesmanAlgorithm(int (*SalesmanFunction)(const SalesmanCities&, LinkedList<int>&),
	int CitiesCount, int RepeatsCount)
{
	Timer BenchmarkTimer;
	LinkedList<int> BenchmarkOutput;
	SalesmanCities BenchmarkInput(CitiesCount);
	for (int MeasurementIndex = 0; MeasurementIndex < RepeatsCount; ++MeasurementIndex)
	{
		FillSalesmanCitiesWithRandomDistances(BenchmarkInput, CITIES_DISTANCE_MINIMUM, CITIES_DISTANCE_MAXIMUM);
		BenchmarkTimer.StartTimer();
		SalesmanFunction(BenchmarkInput, BenchmarkOutput);
		BenchmarkTimer.StopTimer();
		BenchmarkOutput.Clear();
	}
	long double TotalBenchmarkTime = BenchmarkTimer.GetMicroseconds();
	std::cout << (TotalBenchmarkTime / RepeatsCount) << " us";
}
