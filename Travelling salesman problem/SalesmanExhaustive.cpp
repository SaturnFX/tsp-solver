#include "Global.h"
#include "SalesmanExhaustive.h"
#include "SalesmanCommon.h"

#include <limits>

//Funkcja rekurencyjnie sprawdzaj�c� ka�d� mo�liw�
//tras� i zwracaj�ca minimaln�
int SalesmanCheckCombinations(const SalesmanCities& InputData, 
	int SourceCity, int* OptimalConnections, bool* CitiesVisited);

int SalesmanExhaustiveSearch(const SalesmanCities& InputData, LinkedList<int>& OutputRoute)
{
	int* RouteConnections = new int[InputData.CitiesCount];
	bool* CitiesVisited = new bool[InputData.CitiesCount]();
	int RouteDistance = SalesmanCheckCombinations(InputData, 0, RouteConnections, CitiesVisited);
	delete[] CitiesVisited;
	ConvertRouteConnectionsToList(RouteConnections, OutputRoute);
	delete[] RouteConnections;
	return RouteDistance;
}

int SalesmanCheckCombinations(const SalesmanCities& InputData, 
	int SourceCity, int* OptimalConnections, bool* CitiesVisited)
{
	int CurrentDistance = 0;
	int MinimumDistance = std::numeric_limits<int>::max();
	bool AreAllCitiesVisited = true;
	int* CurrentConnections = new int[InputData.CitiesCount];
	//Miasto nr 0 nie jest brane pod uwag�, gdy� znajdywane przez algorytm
	//�cie�ki s� cyklami w kt�rych punkt pocz�tkowy nie ma znaczenia.
	//Wobec tego jest on arbitralnie jako pierwszy
	for (int CurrentCity = 1; CurrentCity < InputData.CitiesCount; ++CurrentCity)
	{
		if (CitiesVisited[CurrentCity]) continue;
		AreAllCitiesVisited = false;
		CitiesVisited[CurrentCity] = true;
		//Dla ka�dego ci�gu miast ju� wybranych znajdujemy inny ci�g z�o�ony
		//z niewybranych miast, kt�rego ��czny dystans jest najmniejszy
		CurrentDistance = SalesmanCheckCombinations(InputData, CurrentCity, CurrentConnections, CitiesVisited);
		CurrentDistance += InputData.CitiesDistances[SourceCity][CurrentCity];
		if (CurrentDistance < MinimumDistance)
		{
			MinimumDistance = CurrentDistance;
			std::copy(CurrentConnections, CurrentConnections + InputData.CitiesCount, 
				OptimalConnections);
			OptimalConnections[SourceCity] = CurrentCity;
		}
		CitiesVisited[CurrentCity] = false;
	}
	if (AreAllCitiesVisited)
	{
		//Gdy wybrano ju� wszystkie miasta oznacza to, �e brakuje jedynie 
		//odcinka ��cz�cego ostatnie dodane miasto z miastem pocz�tkowym
		MinimumDistance = InputData.CitiesDistances[SourceCity][0];
		OptimalConnections[SourceCity] = 0;
	}
	delete[] CurrentConnections;
	return MinimumDistance;
}