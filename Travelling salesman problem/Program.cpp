#include "Global.h"
#include "Exception.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "SalesmanIO.h"
#include "SalesmanExhaustive.h"
#include "SalesmanBB.h"
#include "SalesmanHeldKarp.h"
#include "Benchmark.h"
#include "Utilities.h"

#include <conio.h>
#include <Windows.h>

//Definicja kodu klawisza ENTER
#define ENTER_KEY 0xD

//Ilo�� powt�rze� wykonania algorytmu
//dla pomiar�w wydajno�ci 
#define BENCHMARK_REPEAT_COUNT 100

//Metoda wy�wietlaj�ca menu g��wne programu
void PrintProgramMenu();
//Metoda wy�wietlaj�ca menu algorytm�w
void PrintAlgorithmsMenu();
//Funkcja wy�wietlaj�ca menu inicjalizacji danych
//wej�ciowych algorytm�w rozw. TSP
bool PrintCitiesInitializationMenu(SalesmanCities*& CitiesData);

//Metoda wypisuj�ca komunikat w oknie konsoli
void PrintMessage(std::string MessagePrompt);
//Funkcja pobieraj�ca �a�cuch znak�w z wej�cia konsoli
std::string FetchString(std::string MessagePrompt);
//Funkcja pobieraj�ca �a�cuch znak�w a� do
//klawisza ENTER z wej�cia konsoli
std::string FetchStringWithSpaces(std::string MessagePrompt);
//Metoda wypisuj�ca informacje o programie
void PrintProgramInformation();
//Metoda zatrzymuj�ca wykonanie i programu
//i czekaj�ca na potwierdzenie kontynuacji
void PressEnterKeyToContinue();
//Metoda czyszcz�ca konsol�
void ClearConsole();

//Funkcja startowa programu
int wmain(int argc, wchar_t *argv[], wchar_t *envp[])
{
	SeedRandomGenerator();
	PrintProgramMenu();
	return 0;
}

void PrintProgramMenu()
{
	int FetchedCharacter = 0;
	while (FetchedCharacter != 'c')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Co chcesz zrobi\x86 ?" << std::endl;
		std::cout << "(a) Przetestuj algorytmy rozwi\xA5zuj\xA5""ce aTSP" << std::endl;
		std::cout << "(b) Przeprowad\xAB testy wydajno\x98""ci" << std::endl;
		std::cout << "(c) Wyjd\xAB z programu" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		switch (FetchedCharacter)
		{
			case 'a':
				PrintAlgorithmsMenu();
				break;
			case 'b':
				ClearConsole();
				BenchmarkSalesman(BENCHMARK_REPEAT_COUNT);
				PressEnterKeyToContinue();
		}
	}
}

void PrintAlgorithmsMenu()
{
	int RouteDistance = 0;
	LinkedList<int> OutputRoute;
	SalesmanCities* CitiesData = nullptr;
	std::string ExceptionMessage;
	int FetchedCharacter = 0;
	if (!PrintCitiesInitializationMenu(CitiesData))
	{
		return;
	}
	while (FetchedCharacter != 'f')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Liczba miast: "
			<< CitiesData->CitiesCount << std::endl;
		std::cout << std::endl;
		std::cout << "Co chcesz zrobi\x86 ?" << std::endl;
		std::cout << "(a) Wy\x98wietl dane wej\x98""ciowe" << std::endl;
		std::cout << "(b) Za\x88""aduj nowe dane" << std::endl;
		std::cout << "(c) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu przeszukiwania wyczerpuj\xA5""cego" << std::endl;
		std::cout << "(d) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu Branch & Bound" << std::endl;
		std::cout << "(e) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu Helda-Karpa (programowanie dynamiczne)" << std::endl;
		std::cout << "(f) Porzu\x86 dane i wr\xA2\x86" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		try
		{
			switch (FetchedCharacter)
			{
			case 'a':
				ClearConsole();
				PrintProgramInformation();
				PrintSalesmanCities(*CitiesData);
				PressEnterKeyToContinue();
				break;
			case 'b':
				PrintCitiesInitializationMenu(CitiesData);
				break;
			case 'c':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanExhaustiveSearch(*CitiesData, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'd':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanBranchBoundSolver(*CitiesData, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'e':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanHeldKarpSolver(*CitiesData, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
			}
		}
		catch (const std::exception& Exception)
		{
			ExceptionMessage = "Podczas operacji wyst\xA5pi\x88 nast\xA9puj\xA5""cy b\x88\xA5""d: " ;
			ExceptionMessage += Exception.what();
			PrintMessage(ExceptionMessage);
		}
	}
}

bool PrintCitiesInitializationMenu(SalesmanCities*& CitiesData)
{
	int CitiesNumber = 0;
	int DistanceMinimum = 0;
	int DistanceMaximum = 0;
	std::string FilePathString;
	SalesmanCities* NewCitiesData = nullptr;
	std::string ExceptionMessage;
	int FetchedCharacter = 0;
	while (FetchedCharacter != 'c')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Sk\xA5""d za\x88""adowa\x86 graf miast komiwoja\xBE""era?" << std::endl;
		std::cout << "(a) Wygeneruj dane losowo" << std::endl;
		std::cout << "(b) Za\x88""aduj graf z pliku" << std::endl;
		std::cout << "(c) Wr\xA2\x86 do poprzedniego menu" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		try
		{
			switch (FetchedCharacter)
			{
			case 'a':
				CitiesNumber = std::stoi(FetchString("Podaj liczb\xA9 miast"));
				DistanceMinimum = std::stoi(
					FetchString("Podaj dolne ograniczenie losowanych odleg\x88o\x98""ci mi\xA9""dzy miastami"));
				DistanceMaximum = std::stoi(
					FetchString("Podaj g\xA2rne ograniczenie losowanych odleg\x88o\x98""ci mi\xA9""dzy miastami"));
				NewCitiesData = new SalesmanCities(CitiesNumber);
				FillSalesmanCitiesWithRandomDistances(*NewCitiesData, DistanceMinimum, DistanceMaximum);
				if (CitiesData != nullptr) delete CitiesData;
				CitiesData = NewCitiesData;
				return true;
			case 'b':
				FilePathString = FetchStringWithSpaces("Podaj \x98""cie\xBEk\xA9 do pliku");
				if (LoadSalesmanCitiesFromFile(FilePathString, NewCitiesData) < 0)
				{
					PrintMessage(std::string("Wyst\xA5pi\x88 b\x88\xA5""d"
						" podczas oczytywania zawarto\x98""ci pliku"));
					return false;
				}
				if (CitiesData != nullptr) delete CitiesData;
				CitiesData = NewCitiesData;
				return true;
			}
		}
		catch (const std::exception& Exception)
		{
			ExceptionMessage = "Podczas \x88""adowania danych wyst\xA5pi\x88 nast\xA9puj\xA5""cy b\x88\xA5""d: " ;
			ExceptionMessage += Exception.what();
			PrintMessage(ExceptionMessage);
			break;
		}
	}
	return false;
}

void PrintMessage(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << std::endl;
	PressEnterKeyToContinue();
}

std::string FetchString(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << ": ";
	std::string ResultData;
	std::cin >> ResultData;
	return ResultData;
}

std::string FetchStringWithSpaces(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << ": ";
	std::string ResultData;
	std::getline(std::cin, ResultData);
	return ResultData;
}

void PrintProgramInformation()
{
	std::cout << "Projektowanie efektywnych algorytm\xA2w - zadanie projektowe nr 1" << std::endl;
	std::cout << "Autor: 2020 Micha\x88 Prochera (nr indeksu 248853)" << std::endl;
	std::cout << std::endl;
}

void PressEnterKeyToContinue()
{
	std::cout << std::endl;
	std::cout << "Naci\x98nij ENTER aby kontynowa\x86: ";
	while (_getch() != ENTER_KEY) return;
}

void ClearConsole()
{
	HANDLE ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (ConsoleHandle == INVALID_HANDLE_VALUE) return;
	CONSOLE_SCREEN_BUFFER_INFO ConsoleInfo;
	if (!GetConsoleScreenBufferInfo(ConsoleHandle, &ConsoleInfo)) return;
	DWORD CharactersWritten = 0;
	COORD StartCordinates = {0, 0};
	DWORD CharacterCount = ConsoleInfo.dwSize.X * ConsoleInfo.dwSize.Y;
	if (!FillConsoleOutputCharacterW(ConsoleHandle, L' ', CharacterCount, 
		StartCordinates, &CharactersWritten)) 
	{
		return;
	}
	if (!FillConsoleOutputAttribute(ConsoleHandle, ConsoleInfo.wAttributes,	
		CharacterCount,	StartCordinates, &CharactersWritten)) 
	{
		return;
	}
	SetConsoleCursorPosition(ConsoleHandle, StartCordinates);
}