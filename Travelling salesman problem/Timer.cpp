#include "Global.h"
#include "Timer.h"

#include <Windows.h>

Timer::Timer()
{
	StartTick = 0;
	ElapsedTicks = 0;
	LARGE_INTEGER FrequencyData;
	QueryPerformanceFrequency(&FrequencyData);
	Frequency = FrequencyData.QuadPart;
}

int Timer::GetMiliseconds() const
{
	__int64 ElapsedTime = ElapsedTicks * 1000; 
	ElapsedTime = ElapsedTime / Frequency;
	return (int) ElapsedTime;
}

int Timer::GetMicroseconds() const
{
	__int64 ElapsedTime = ElapsedTicks * 1000000;
	ElapsedTime = ElapsedTime / Frequency;
	return (int) ElapsedTime;
}

void Timer::StartTimer()
{
	LARGE_INTEGER StartTickData;
	QueryPerformanceCounter(&StartTickData);
	StartTick = StartTickData.QuadPart;
}

void Timer::StopTimer()
{
	LARGE_INTEGER EndTickData;
	QueryPerformanceCounter(&EndTickData);
	ElapsedTicks = ElapsedTicks + EndTickData.QuadPart - StartTick;
}

void Timer::ResetTimer()
{
	ElapsedTicks = 0;
}