#include "Global.h"
#include "Utilities.h"

#include <cstdlib>
#include <ctime> 

void SeedRandomGenerator()
{
	srand(time(nullptr));
}

int GetRandomInteger(int MinimumValue, int MaximumValue)
{
	return (MinimumValue + (rand() % (MaximumValue - MinimumValue + 1)));
}

int CalculateFactorial(int InputNumber)
{
	int ResultValue = 1;
	for (int Multiplier = 2; Multiplier <= InputNumber; ++Multiplier)
	{
		ResultValue *= Multiplier;
	}
	return ResultValue;
}