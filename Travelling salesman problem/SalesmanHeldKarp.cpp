#include "Global.h"
#include "SalesmanHeldKarp.h"

#include <limits>

//Maksymalna ilo�� miast mo�liwa 
//do przetworzenia algorytmem 
//Helda-Karpa bior�c pod uwag� za�o�one 
//ograniczenie zu�ywanej pami�ci r�wne 2 GB
//
//Obliczenia wykonano przez rozwi�zanie 
//nast�puj�cego r�wnania:
//8 * (n - 1) * 2^(n - 1) = 2 * (1024^3)
#define HK_TSP_MAXIMUM_CITIES 24

//Struktura opisuj�ca fragment trasy
//komiwoja�era zaczynaj�cego si� od miasta 0
//zawieraj�ca ��czn� d�ugo�� odcinka 
//i przedostatnie miasto
struct SalesmanSubroute
{
	int Distance;
	int LastCity;

	//Konstruktor tworz�cy niemo�liwy 
	//fragment trasy o niesko�czonej d�ugo�ci
	SalesmanSubroute();
	//Konstruktor tworz�cy pocz�tkowy fragment trasy
	//o pocz�tku w mie�cie 0
	SalesmanSubroute(const SalesmanCities& InputData, int EndCity);
	//Konstruktor tworz�cy nowy odcinek trasy 
	//z istniej�cego pod��czaj�c do niego nowe miasto
	SalesmanSubroute(const SalesmanCities& InputData, 
		SalesmanSubroute* SubroutesArray, int StartCity, int EndCity);
};

int SalesmanHeldKarpSolver(const SalesmanCities& InputData, LinkedList<int>& OutputRoute)
{
	if (InputData.CitiesCount > HK_TSP_MAXIMUM_CITIES)
	{
		throw AlgorithmException("Input cities graph is too big");
	}
	int SubrouteIndex = 1;
	int NextSingleCityIndex = 0;
	int NextSingleCitySubroute = 1;
	int EndCity = 0;
	int EndCityBits = 0;
	int IntermediateSubrouteIndex = 0;
	int IntermediateCity = 0;
	int IntermediateCityBits = 0;
	SalesmanSubroute MinimumSubrouteValue;
	SalesmanSubroute CurrentSubrouteValue;
	SalesmanSubroute* EndSubroutesArray = nullptr;
	SalesmanSubroute* IntermediateSubroutesArray = nullptr;
	int MaximumSubrouteCites = InputData.CitiesCount - 1;
	int SubroutesCount = (1 << MaximumSubrouteCites);
	SalesmanSubroute** SubroutesArray = new SalesmanSubroute*[SubroutesCount];
	//Przegl�danie wszytkich mo�liwych pozbior�w miast 
	//z wy��czeniem miasta 0 i aktualizowanie powi�zanych
	//fragment�w tras. Pozbiory te oznaczane s� maskami bitowymi,
	//w kt�rych pozycja danego bitu oznacza numer miasta, a jego 
	//warto�� okre�la wyst�powanie miasta lub jego brak - odpowiednio
	//dla 1 oraz 0.
	for (SubrouteIndex = 1; SubrouteIndex < SubroutesCount; ++SubrouteIndex)
	{
		EndSubroutesArray = new SalesmanSubroute[MaximumSubrouteCites]();
		SubroutesArray[SubrouteIndex] = EndSubroutesArray;
		//Sprawdzenie czy dany zbi�r miast nie jest
		//jednoelementowy
		if (SubrouteIndex != NextSingleCitySubroute)
		{
			EndCity = 0;
			EndCityBits = SubrouteIndex;
			//Iteracja po miastach znajduj�cych si�
			//w danym zbiorze i rozwa�enie ka�dego
			//z nich jako miasta ko�cowego nowego
			//odcinka trasy komiwoja�era
			while (EndCityBits)
			{
				if (EndCityBits & 1)
				{
					MinimumSubrouteValue = SalesmanSubroute();
					IntermediateSubrouteIndex = SubrouteIndex & (~(1 << EndCity));
					IntermediateSubroutesArray = SubroutesArray[IntermediateSubrouteIndex];
					IntermediateCity = 0;
					IntermediateCityBits = IntermediateSubrouteIndex;
					//Iteracja po miastach z tego samego
					//zbioru pomijaj�c ko�cowe, wybrane wcze�niej.
					//Ka�de rozwa�ane jest jako pocz�tek
					//nowego odcinka trasy i zarazem punkt
					//��cz�cy miasto ko�cowe z reszt� zbioru.
					//Ze wszystkich wybierane jest to zapewniaj�ce
					//najni�szy ca�kowity koszt trasy.
					while (IntermediateCityBits)
					{
						if (IntermediateCityBits & 1)
						{
							CurrentSubrouteValue = SalesmanSubroute(InputData, 
								IntermediateSubroutesArray, IntermediateCity, EndCity);
							if (CurrentSubrouteValue.Distance < MinimumSubrouteValue.Distance)
							{
								MinimumSubrouteValue = CurrentSubrouteValue;
							}
						}
						++IntermediateCity;
						IntermediateCityBits >>= 1;
					}
					EndSubroutesArray[EndCity] = MinimumSubrouteValue;
				}
				++EndCity;
				EndCityBits >>= 1;
			}
		}
		else
		{
			//Dla zbioru jednoelementowego uzupe�niane jest
			//miejsce odpowiadaj�ce jedynemu miastu 
			//struktur� zawieraj�c� jego odleg�o�� od miasta 0
			EndSubroutesArray[NextSingleCityIndex] = SalesmanSubroute(InputData, NextSingleCityIndex);
			++NextSingleCityIndex;
			NextSingleCitySubroute <<= 1;
		}
	}
	//W�r�d tras zawieraj�cych wszystkie odcinki mi�dzy
	//miastami poza ostatnim szukana jest taka, kt�ra
	//po dodaniu ostatniego po��czenia b�dzie mia�a
	//najni�sz� d�ugo��
	MinimumSubrouteValue = SalesmanSubroute();
	IntermediateSubrouteIndex = SubroutesCount - 1;
	IntermediateSubroutesArray = SubroutesArray[IntermediateSubrouteIndex];
	for (IntermediateCity = 0; IntermediateCity < MaximumSubrouteCites; ++IntermediateCity)
	{
		CurrentSubrouteValue = SalesmanSubroute(InputData,
			IntermediateSubroutesArray, IntermediateCity, -1);
		if (CurrentSubrouteValue.Distance < MinimumSubrouteValue.Distance)
		{
			MinimumSubrouteValue = CurrentSubrouteValue;
		}
	}
	//Sk�adanie minimalnej trasy wykorzystuj�c
	//drugie pole w strukturze opisuj�cej 
	//poprzednio odwiedzone miasto
	EndCity = MinimumSubrouteValue.LastCity;
	OutputRoute.AddFirst(0);
	while (EndCity >= 0)
	{
		OutputRoute.AddFirst(EndCity + 1);
		IntermediateCity = SubroutesArray[IntermediateSubrouteIndex][EndCity].LastCity;
		IntermediateSubrouteIndex &= ~(1 << EndCity);
		EndCity = IntermediateCity;
	}
	OutputRoute.AddFirst(0);
	//Dealokacja pami�ci dynamicznej u�ywanej
	//przez algorytm
	for (SubrouteIndex = 1; SubrouteIndex < SubroutesCount; ++SubrouteIndex)
	{
		delete[] SubroutesArray[SubrouteIndex];
	}
	delete[] SubroutesArray;
	return MinimumSubrouteValue.Distance;
}

SalesmanSubroute::SalesmanSubroute()
	:Distance(std::numeric_limits<int>::max()), LastCity(std::numeric_limits<int>::max())
{}

SalesmanSubroute::SalesmanSubroute(const SalesmanCities& InputData, int EndCity)
	:Distance(InputData.CitiesDistances[0][EndCity + 1]), LastCity(-1)
{}

SalesmanSubroute::SalesmanSubroute(const SalesmanCities& InputData, 
	SalesmanSubroute* SubroutesArray, int StartCity, int EndCity)
	:Distance(SubroutesArray[StartCity].Distance 
		+ InputData.CitiesDistances[StartCity + 1][EndCity + 1]), 
	LastCity(StartCity)
{}