#pragma once
#include "Global.h"

/*
* Modu� Benchmark
*
* Umo�liwia testowanie wydajno�ci
* dzia�ania zaimplementowanych algorytm�w
* rozwi�zuj�cych TSP dla r�nych parametr�w
*/

//Mierzy i wy�wietla �rednie czasy wykonania
//algorytm�w rozwi�zuj�cych TSP dla losowych danych
//o okre�lonej wielko�ci oraz podanej ilo�ci powt�rze�
void BenchmarkSalesman(int RepeatsCount);