#pragma once
#include "Global.h"

/*
* Klasa: Timer
*
* Klasa umo�liwiaj�ca mierzenie
* czasu, kt�ry up�yn�� podczas
* dzia�ania programu.
*/

class Timer 
{
private:
	//Warto�� licznika wydajno�ci
	//w chwili rozpocz�cia ostatniej
	//cz�ci pomiaru
	__int64 StartTick;
	//Pobrana z systemu warto�� okre�laj�ca
	//zmian� warto��i licznika w ci�gu 1 s
	__int64 Frequency;
	//Ca�kowita zmiana warto�ci licznika
	//podczas wykonanego pomiaru
	__int64 ElapsedTicks;
public:
	Timer();
	//Metoda umo�liwiaj�ca pobranie
	//zmierzonego czasu w milisekundach
	int GetMiliseconds() const;
	//Metoda umo�liwiaj�ca pobranie
	//zmierzonego czasu w mikrosekundach
	int GetMicroseconds() const;
	//Metoda rozpoczynaj�ca pomiar czasu
	void StartTimer();
	//Metoda wstrzymuj�ca pomiar czasu
	void StopTimer();
	//Metoda zeruj�ca wykonany pomiar
	void ResetTimer();
};
