#pragma once
#include "Global.h"
#include <exception>

/*
* Klasa: ApplicationException
*
* Reprezentuje wyj�tki jakie mog� by�
* zwracane przez aplikacj�
*/

class ApplicationException : public std::exception
{
private:
	//�a�cuch zawieraj�cy opis b��du
	//kt�ry wyst�pi�
	const std::string Mesage;
public:
	//Konstruktor przyjmuj�cy w parametrze
	//opis b��du
	ApplicationException(const std::string Description);
	//Funkcja zwracaj�ca opis b��du 
	virtual const char* what() const throw ();
};

/*
* Klasa: ContainerException
*
* Reprezentuje wyj�tki zwracane
* przez struktury
*/

class ContainerException : public ApplicationException
{
public:
	ContainerException(const std::string Description)
		: ApplicationException(Description)
	{}
};

/*
* Klasa: AlgorithmException
*
* Reprezentuje wyj�tki zwracane
* przez algorytmy
*/

class AlgorithmException : public ApplicationException
{
public:
	AlgorithmException(const std::string Description)
		: ApplicationException(Description)
	{}
};