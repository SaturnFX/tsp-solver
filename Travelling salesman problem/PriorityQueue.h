#pragma once
#include "Global.h"
#include "AbstractContainer.h"
#include "Exception.h"
#include "Utilities.h"

inline int GetParentIndex(int Index);
inline int GetLeftChildIndex(int Index);
inline int GetRightChildIndex(int Index);

/*
* Klasa: PriorityQueue
*
* Kolejka priorytetowa typu minimum
* oparta na kopcu binarnym
*/
template <typename T>
class PriorityQueue : public AbstractContainer
{
private:
	T* HeapArray;
	int HeapMaximum;
	bool (*CompareFunction)(const T&, const T&);

	//Metoda tworząca kopiec
	//o zadanej długości
	void CreateHeap(int MaximumLength)
	{
		HeapMaximum = MaximumLength;
		HeapArray = new T[HeapMaximum + 1];
		*HeapArray = 0;
	}

	//Metoda naprawiająca własność
	//kopca przechodząc w górę
	void RepairHeapIndexUp(int ItemIndex)
	{
		int CurrentIndex = ItemIndex;
		int ParentIndex = GetParentIndex(CurrentIndex);
		T ItemValue = HeapArray[ItemIndex];
		while ((CurrentIndex > 1) && CompareFunction(ItemValue, HeapArray[ParentIndex]))
		{
			HeapArray[CurrentIndex] = HeapArray[ParentIndex];
			CurrentIndex = ParentIndex;
			ParentIndex = GetParentIndex(CurrentIndex);
		}
		HeapArray[CurrentIndex] = ItemValue;
	}

	//Metoda naprawiająca własność
	//kopca przechodząc w dół
	void RepairHeapIndexDown(int ItemIndex)
	{
		int SmallestIndex = ItemIndex;
		int LeftIndex = GetLeftChildIndex(ItemIndex);
		int RightIndex = GetRightChildIndex(ItemIndex);
		if ((LeftIndex <= DataLength) && (CompareFunction(HeapArray[LeftIndex], HeapArray[SmallestIndex])))
		{
			SmallestIndex = LeftIndex;
		}
		if ((RightIndex <= DataLength) && (CompareFunction(HeapArray[RightIndex], HeapArray[SmallestIndex])))
		{
			SmallestIndex = RightIndex;
		}
		if (SmallestIndex != ItemIndex)
		{
			SwapItems<T>(HeapArray, SmallestIndex, ItemIndex);
			RepairHeapIndexDown(SmallestIndex);
		}
	}

	//Funkcja definiująca domyślny
	//sposób porównywania elementów
	//w kolejce
	static bool DefaultCompare(const T& FirstItem, const T& SecondItem)
	{
		return FirstItem < SecondItem;
	}

public:
	//Konstruktor kolejki z domyślnym
	//sposobem porównywania elementów
	//poprzez użycie operatorów
	PriorityQueue(int MaximumLength)
		:CompareFunction(PriorityQueue::DefaultCompare)
	{
		CreateHeap(MaximumLength);
	}
	
	//Konstruktor kolejki pozwalający
	//zdefiniować sposób porównywania
	//elementów
	PriorityQueue(int MaximumLength, bool (*Compare)(const T&, const T&))
		:CompareFunction(Compare)
	{
		CreateHeap(MaximumLength);
	}

	//Funkcja pobierająca
	//minimalny element z kopca
	T GetMinimum() const
	{
		ValidateLength();
		return HeapArray[1];
	}

	//Funkcja pobierająca 
	//i usuwająca element
	//minimalny
	T ExtractMinimum()
	{
		ValidateLength();
		T MinimumValue = HeapArray[1];
		SwapItems<T>(HeapArray, 1, DataLength);
		--DataLength;
		RepairHeapIndexDown(1);
		return MinimumValue;
	}

	//Metoda dodająca nowy 
	//element do kopca
	void Add(T Value)
	{
		if (DataLength == HeapMaximum) 
		{
			throw ContainerException("There is no more space is priority queue");
		}
		HeapArray[++DataLength] = Value;
		RepairHeapIndexUp(DataLength);
	}

	//Metoda czyszcząca kopiec
	void Clear()
	{
		delete[] HeapArray;
		CreateHeap(HeapMaximum);
		AbstractContainer::Clear();
	}

	~PriorityQueue()
	{
		delete[] HeapArray;
	}
};

inline int GetParentIndex(int Index)
{
	return Index >> 1;
}

inline int GetLeftChildIndex(int Index) 
{
	return Index << 1;
}

inline int GetRightChildIndex(int Index)
{
	return (Index << 1) + 1;
}
