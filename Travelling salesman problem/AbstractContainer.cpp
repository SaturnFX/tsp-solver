#include "Global.h"
#include "AbstractContainer.h"
#include "Exception.h"

AbstractContainer::AbstractContainer()
{
	DataLength = 0;
}

int AbstractContainer::GetLength() const
{
	return DataLength;
}

void AbstractContainer::ValidateLength() const
{
	if (DataLength > 0) return;
    throw ContainerException(std::string("This container is empty"));
}

void AbstractContainer::Clear()
{
	DataLength = 0;
}

AbstractContainer::~AbstractContainer() {}